#!/usr/bin/env python
# -*- coding: utf8 -*-

# ultraviolance distutils hack
import re
import distutils.versionpredicate
distutils.versionpredicate.re_validPackage = re.compile(r"^\s*([a-z_-]+)(.*)", re.IGNORECASE)

from distutils.core import setup

packages = {
    'evcurl' : 'src/evcurl',
}


setup ( 
        name= 'evcurl',
        version='0.3.2',
        packages=packages,
        package_dir = packages,
        author='Evgeny Turnaev',
        author_email='turnaev.e@gmail.com',
        description=' pyev + pycurl ',
        long_description=""" pyev + pycurl """,

        classifiers = ['devel'],
        options={'build_pkg': {'name_prefix': True,
                               'python_min_version': 2.7,
                              }},
        requires = [
                    'pycurl',
                    'pyev',
                    'xutils'
        ],
        provides = packages.keys(),
        )



