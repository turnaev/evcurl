#!/usr/bin/env python

from logging import debug, warning, info, error, critical

import os
import signal
from codecs import getencoder
from cStringIO import StringIO
from pprint import pformat


import pycurl
from pycurl import TIMEOUT_MS
from pyev import EV_READ, EV_WRITE, EV_IO

from xutils.string import bstr

# this one for curl
signal.signal(signal.SIGPIPE, signal.SIG_IGN)


EV_CURL_EVENT_MAP = {
    pycurl.POLL_NONE: 0,
    pycurl.POLL_IN: EV_READ,
    pycurl.POLL_OUT: EV_WRITE,
    pycurl.POLL_INOUT: EV_IO,
}


curl_options = {
    "GET": pycurl.HTTPGET,
    "POST": pycurl.POST,
    "PUT": pycurl.UPLOAD,
    "HEAD": pycurl.NOBODY,
}


def get_curl(_debug = False):
    c = pycurl.Curl()
    if _debug:
        c.setopt(pycurl.VERBOSE, 1)
        c.setopt(pycurl.DEBUGFUNCTION, curl_debug)
    c.setopt(pycurl.FOLLOWLOCATION, 0)
    c.setopt(pycurl.MAXREDIRS, 0)
    c.setopt(pycurl.NOSIGNAL, 1)
    c.setopt(pycurl.DNS_CACHE_TIMEOUT, 60 * 30)
    if __debug__:
        debug("get_curl returns: %s" % repr(c))
    return c

def curl_debug(debug_type, msg):
    warning("debug: %s %s" % (repr(debug_type), repr(msg)))


def format_curl(c):
    return "%s, url: %s, fetch_struct: %s" % (c, c.url, c.fetch_struct)


class EvCurl(object):

    def __init__(self, loop, keep_alive = 20, curl_pool = 20, fetch_timeout = 60):

        self.fetch_timeout = fetch_timeout
        self.loop = loop
        self.curl_pool = curl_pool

        self.free_curls = []
        self.timeout_timers = {}

        for x in range(curl_pool):
            c = get_curl()
            self.free_curls.append(c)

        self.watchers = {}
        # timer to call perform() on multi
        self.curl_timer = None

        multi = pycurl.CurlMulti()
        multi.setopt(pycurl.M_MAXCONNECTS, keep_alive)
        multi.setopt(pycurl.M_SOCKETFUNCTION, self.curl_socket_cb)
        multi.setopt(pycurl.M_TIMERFUNCTION, self.curl_timer_cb)
        self.multi = multi


    def cleanup(self):
        try:
            for fd, watcher in self.watchers.iteritems():
                watcher.stop()

            # stop all fetchers and their timeouts
            for c in self.timeout_timers.keys()[:]:
                self._push_free(c)

        except:
            critical("ev curl cleanup error", exc_info = True)

        self.timeout_timers = {}
        self.watchers = {}
        if self.curl_timer:
            self.curl_timer.stop()


    def curl_socket_cb(self, event, fd, multi, data, event_map = EV_CURL_EVENT_MAP):
        watchers = self.watchers
        if event == pycurl.POLL_REMOVE:
            # curl wants to stop poll on fd
            watchers[fd].stop()
            del watchers[fd]
            return
        else:
            ev_event = event_map[event]
            if fd not in watchers:
                # create new watcher for socket
                watcher = self.loop.io(fd, ev_event, self.ev_event_cb)
                watcher.start()
                watchers[fd] = watcher
                if __debug__:
                    debug("created watcher %s feed event for fd: %d" % (repr(watcher), fd))
                watcher.feed(ev_event)
            else:
                watcher = watchers[fd]
                watcher.stop()
                watcher.set(fd, ev_event)
                watcher.start()
                if __debug__:
                    debug("updated watcher: %s feed event for fd: %d" % (repr(watcher), fd))
                watcher.feed(ev_event)

    def curl_timer_cb(self, msecs):
        if msecs == -1:
            return
        if __debug__:
            debug("curl timer set after: %0.6f" % (msecs/1000.0))
        if self.curl_timer:
            if __debug__:
                debug("curl timer stop.")
            self.curl_timer.stop()
            self.curl_timer.set(msecs/1000.0, 0)
        else:
            # first set.
            self.curl_timer = self.loop.timer(msecs/1000.0, 0, self.handle_curl_timeout)
        self.curl_timer.start()


    def handle_curl_timeout(self, timer, timer_events):
        multi = self.multi
        try:
            if __debug__:
                debug("handle_curl_timeout: timer: %s, timer_events: %s" % (timer, timer_events))
            while True: 
                try:
                    ret, num_handles = multi.socket_action(pycurl.SOCKET_TIMEOUT, 0)
                except pycurl.error, e:
                    ret = e.args[0]
                if ret != pycurl.E_CALL_MULTI_PERFORM:
                    break
            self.process_curl_complete_handles()
        except:
            error("",exc_info = True)


    def _push_free(self, c):
        self.multi.remove_handle(c)

        if len(self.free_curls) < self.curl_pool:
            self.free_curls.append(c)

        self.timeout_timers[c].stop()
        del c.fetch_struct # break back reference to current req
        del c.url
        del self.timeout_timers[c]
        if not self.timeout_timers:
            # not fetches currenly
            if __debug__:
                debug("no fetches  - stop curl timer.")
            self.curl_timer.stop()


    def process_curl_complete_handles(self):
        multi = self.multi
        while 1:
            num_q, ok_list, err_list = multi.info_read()
            if __debug__:
                debug("process_curl_complete_handles: %s, %s, %s" % (num_q, ok_list, err_list))
            for c in ok_list:
                status_code = c.getinfo(pycurl.HTTP_CODE)
                if status_code != 200:
                    warning("status code: %s for url: %s " % (status_code, repr(c.url)))
                else:
                    debug("success: %s http_status: %s" % (repr(c.url), status_code))
                cb = c.fetch_struct.get('success_callback', None)
                if cb:
                    try:
                        cb(c, {})
                    except:
                        error("ev_curl success cb error", exc_info = True)
                self._push_free(c)
            for c, errnum, errmsg in err_list:
                error("fetch fail: (%s, %s) for curl: %s"  % (errnum, errmsg, format_curl(c)))
                cb = c.fetch_struct.get('error_callback', None)
                if cb:
                    info = {'errno': errnum, 'errmsg': errmsg}
                    try:
                        cb(c, info)
                    except:
                        error("ev_curl error cb error", exc_info = True)
                self._push_free(c)
            if num_q == 0:
                break

    def ev_event_cb(self, watcher, events):
        multi = self.multi
        try:
            if __debug__:
                debug("ev_event_cb: fd:%s, ev_event:%s" % (watcher.fd, events))
            curl_action = 0
            if events & EV_READ:
                curl_action |= pycurl.CSELECT_IN
            if events & EV_WRITE:
                curl_action |= pycurl.CSELECT_OUT
            while 1:
                try:
                    ret, num_handles = multi.socket_action(watcher.fd, 0)
                except pycurl.error, e:
                    ret = e.args[0]
                if ret != pycurl.E_CALL_MULTI_PERFORM:
                    break
            self.process_curl_complete_handles()
        except:
            error("", exc_info = True)


    def fetch(self, fetch_struct):
        if __debug__:
            debug("fetch: %s" % repr(fetch_struct))
        try:
            c = self._fetch_struct(fetch_struct)
            try:
                self.multi.socket_action(pycurl.SOCKET_TIMEOUT, 0)
                self.process_curl_complete_handles()
            except:
                self.int_curl(c)
                raise
            return c
        except:
            critical("fetch error", exc_info = True)

    def fetch_timeout_cb(self, timer, events):
        try:
            warning("fetch timeout: %s" % format_curl(timer.data))
            c = timer.data
            cb = c.fetch_struct.get('error_callback', None)
            if cb:
                try:
                    cb(c, {}, timeout = True)
                except:
                    error("ev_curl timeout cb error", exc_info = True)
            self._push_free(c)
        except:
            error("", exc_info = True)


    def _fetch_struct(self, fetch_struct, enc = getencoder('utf-8')):
        try:
            c = self.free_curls.pop()
        except IndexError:
            # no free curls
            c = get_curl()

        if __debug__:
            debug("ev_curl fetch_struct: %s" % pformat(fetch_struct))

        if fetch_struct.get('debug', False):
            c.setopt(pycurl.VERBOSE, 1)
            c.setopt(pycurl.DEBUGFUNCTION, curl_debug)
        else:
            c.setopt(pycurl.VERBOSE, 0)
            c.setopt(pycurl.DEBUGFUNCTION, curl_debug)

        timeout = fetch_struct.get('timeout', self.fetch_timeout)
        setopt = c.setopt

        # XXX so ugly -- pycurl realy should have easy_curl_reset()
        for o in curl_options.values():
            setopt(o, False)

        method = fetch_struct.get('method', None)
        if method:
            if __debug__:
                debug("http_method: %s" % method)
        else:
            method = 'GET'

        method_opt = curl_options[method]
        setopt(method_opt, True)

        if __debug__:
            debug("free curls left: %d" % len(self.free_curls))

        url = bstr(fetch_struct['url'], enc)
        if __debug__:
            debug("fetch url: %s" % repr(url))

        c.url = url
        c.fetch_struct = fetch_struct

        ssl_verify = fetch_struct.get('ssl_verify', False)
        if url.startswith("https"):
            if ssl_verify:
                if __debug__:
                    debug("ssl verify - True")
                setopt(pycurl.SSL_VERIFYPEER, 1)
                setopt(pycurl.SSL_VERIFYHOST, 2)
            else:
                if __debug__:
                    debug("ssl verify - False")
                setopt(pycurl.SSL_VERIFYPEER, 0)
                setopt(pycurl.SSL_VERIFYHOST, 0)

        setopt(c.URL, url)
        setopt(TIMEOUT_MS, int(timeout * 1000))
        c.read_buf = StringIO()
        c.headers_buf = StringIO()
        setopt(c.WRITEFUNCTION, c.read_buf.write)
        setopt(c.HEADERFUNCTION, c.headers_buf.write)

        if method == "POST" or method == 'PUT':

            request_buffer = fetch_struct['body']
            request_buffer.seek(0, os.SEEK_END)
            body_length = request_buffer.tell()
            request_buffer.seek(0)

            setopt(pycurl.READFUNCTION, request_buffer.read)
            if method == "POST":
                def ioctl(cmd):
                    if cmd == c.IOCMD_RESTARTREAD:
                        request_buffer.seek(0)
                setopt(pycurl.IOCTLFUNCTION, ioctl)
                setopt(pycurl.POSTFIELDSIZE, body_length)
            else:
                setopt(pycurl.INFILESIZE, body_length)

        headers = {
                     #"Connection" : "Keep-Alive",
                     "Connection" : "Close", # Close connection until ev_curl moved into separate thread
                     "Accept-Encoding" : "gzip",
                     'User-Agent' : '',
                     'Accept' : '',
                  }

        if 'headers' in fetch_struct:
            headers.update(dict( (bstr(k,enc), bstr(v, enc)) for k,v in fetch_struct['headers'].iteritems()))

        setopt(pycurl.HTTPHEADER, ['%s: %s' % (k,v) for k,v in headers.iteritems()])
        self.multi.add_handle(c)

        timer = self.loop.timer(timeout, 0, self.fetch_timeout_cb, c)
        timer.start()
        self.timeout_timers[c] = timer




